package net.pl3x.pl3xlibs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import joebkt.IntegerCoordinates;
import joebkt.Packet_BlockChange;
import joebkt.WorldServer;
import net.minecraft.server.MinecraftServer;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import net.pl3x.pl3xlibs.scheduler.Scheduler;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Attribute;

import PluginReference.MC_Block;
import PluginReference.MC_Entity;
import PluginReference.MC_EntityType;
import PluginReference.MC_ItemStack;
import PluginReference.MC_ItemType;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_Skeleton;
import PluginReference.MC_SkeletonType;
import PluginReference.MC_World;
import PluginReference.PluginInfo;
import WrapperObjects.BlockWrapper;
import WrapperObjects.WorldWrapper;
import WrapperObjects.Entities.PlayerWrapper;
import _Pl3xLibs.MyPlugin;

public class Pl3xLibs {
	public static String colorizeConsole(String message) {
		message = message.replaceAll("(?i)&(0)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.BLACK).boldOff().toString());
		message = message.replaceAll("(?i)&(1)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.BLUE).boldOff().toString());
		message = message.replaceAll("(?i)&(2)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.GREEN).boldOff().toString());
		message = message.replaceAll("(?i)&(3)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.CYAN).boldOff().toString());
		message = message.replaceAll("(?i)&(4)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.RED).boldOff().toString());
		message = message.replaceAll("(?i)&(5)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.MAGENTA).boldOff().toString());
		message = message.replaceAll("(?i)&(6)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.YELLOW).boldOff().toString());
		message = message.replaceAll("(?i)&(7)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.WHITE).boldOff().toString());
		message = message.replaceAll("(?i)&(8)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.BLACK).bold().toString());
		message = message.replaceAll("(?i)&(9)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.BLUE).bold().toString());
		message = message.replaceAll("(?i)&(a)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.GREEN).bold().toString());
		message = message.replaceAll("(?i)&(b)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.CYAN).bold().toString());
		message = message.replaceAll("(?i)&(c)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.RED).bold().toString());
		message = message.replaceAll("(?i)&(d)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.MAGENTA).bold().toString());
		message = message.replaceAll("(?i)&(e)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.YELLOW).bold().toString());
		message = message.replaceAll("(?i)&(f)", Ansi.ansi().a(Attribute.RESET).fg(Ansi.Color.WHITE).bold().toString());
		message = message.replaceAll("(?i)&(k)", Ansi.ansi().a(Attribute.BLINK_SLOW).toString());
		message = message.replaceAll("(?i)&(l)", Ansi.ansi().a(Attribute.UNDERLINE_DOUBLE).toString());
		message = message.replaceAll("(?i)&(m)", Ansi.ansi().a(Attribute.STRIKETHROUGH_ON).toString());
		message = message.replaceAll("(?i)&(n)", Ansi.ansi().a(Attribute.UNDERLINE).toString());
		message = message.replaceAll("(?i)&(o)", Ansi.ansi().a(Attribute.ITALIC).toString());
		message = message.replaceAll("(?i)&(r)", Ansi.ansi().a(Attribute.RESET).toString());
		return message;
	}

	/**
	 * Convert color and style raw text codes (&a) into Minecraft color codes
	 * 
	 * @param string String containing color codes
	 * @return Colorized string
	 */
	public static String colorize(String string) {
		return string.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}

	/**
	 * Remove all color and style codes from a string. Both Minecraft and raw text color/style codes will be removed
	 * 
	 * @param string String containing color codes
	 * @return Decolorized string
	 */
	public static String decolorize(String string) {
		return stripColors(stripStyles(string));
	}

	/**
	 * Remove all style codes from a string. Both Minecraft and raw text style codes will be removed
	 * 
	 * @param string String containing style codes
	 * @return Destylized string
	 */
	public static String stripStyles(String string) {
		return string.replaceAll("(?i)&([k-or])", "").replaceAll("(?i)\u00a7([k-or])", "");
	}

	/**
	 * Remove all color codes from a string. Both Minecraft and raw text style codes will be removed
	 * 
	 * @param string String containing style codes
	 * @return Destylized string
	 */
	public static String stripColors(String string) {
		return string.replaceAll("(?i)&([a-f0-9])", "").replaceAll("(?i)\u00a7([a-f0-9])", "");
	}

	/**
	 * Check if a string contains color codes.
	 * 
	 * @param string String to check
	 * @return True if color codes are found, false if not.
	 */
	public static boolean hasColor(String string) {
		return !string.equals(stripColors(string));
	}

	/**
	 * Check if a string contains style codes.
	 * 
	 * @param string String to check
	 * @return True if style codes are found, false if not.
	 */
	public static boolean hasStyle(String string) {
		return !string.equals(stripStyles(string));
	}

	/**
	 * Join a string list together into a single string
	 * 
	 * @param list A String[] list
	 * @param joiner String joiner
	 * @param start The starting index
	 * @return Single joined string
	 */
	public static String join(String[] list, String joiner, int start) {
		return join(Arrays.asList(list), joiner, start);
	}

	/**
	 * Join a set of strings together into a single string
	 * 
	 * @param set Set of strings
	 * @param joiner String joiner
	 * @param start The starting index
	 * @return Single joined string
	 */
	public static String join(Set<String> list, String joiner, int start) {
		return join(list.toArray(new String[0]), joiner, start);
	}

	/**
	 * Join a list of strings together into a single string
	 * 
	 * @param list List of strings
	 * @param joiner String joiner
	 * @param start The starting index
	 * @return Single joined string
	 */
	public static String join(List<String> list, String joiner, int start) {
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < list.size(); i++) {
			if (i != start) {
				bldr.append(joiner);
			}
			bldr.append(list.get(i));
		}
		return bldr.toString();
	}

	/**
	 * Gets the limit set in a permission node. Max limit is 1024.
	 * 
	 * @param player The player to check permission on
	 * @param node The limit node
	 * @return Integer limit of node, or -1 if unlimited
	 */
	public static Integer getLimit(MC_Player player, String node) {
		int limit = 0;
		int maxlimit = 1024;
		if (player.hasPermission(node + ".*")) {
			return -1;
		}
		for (int i = 0; i < maxlimit; i++) {
			if (player.hasPermission(node + "." + i) && i > limit) {
				limit = i;
			}
		}
		return limit;
	}

	/**
	 * Converts string to MC_Location
	 * 
	 * @param string String representation of a location (x,y,z,dimension,yaw,pitch)
	 * @return MC_Location or null if string is invalid
	 */
	public static MC_Location stringToLocation(String string) {
		String[] rawSplit = string.split(",");
		double x, y, z;
		int dimension;
		float yaw, pitch;
		try {
			x = Double.valueOf(rawSplit[0]);
			y = Double.valueOf(rawSplit[1]);
			z = Double.valueOf(rawSplit[2]);
			dimension = Integer.valueOf(rawSplit[3]);
			yaw = Float.valueOf(rawSplit[4]);
			pitch = Float.valueOf(rawSplit[5]);
		} catch (Exception e) {
			return null;
		}
		return new MC_Location(x, y, z, dimension, yaw, pitch);
	}

	/**
	 * Converts MC_Location to string
	 * 
	 * @param location An MC_Location to convert
	 * @return String representation of location
	 */
	public static String locationToString(MC_Location location) {
		return location.x + "," + location.y + "," + location.z + "," + location.dimension + "," + location.yaw + "," + location.pitch;
	}

	/**
	 * Crude method for finding a safe location from suffocation
	 * 
	 * @param location Currrent MC_Location
	 * @return MC_Location with Y coordinate safer spot
	 */
	public static MC_Location getSafeLocation(MC_Location location) {
		int dimension = location.dimension;
		double x = location.getBlockX();
		int y = location.getBlockY();
		double z = location.getBlockZ();
		location.x = x + 0.5;
		location.z = z + 0.5;
		if (isSafeLocation(new MC_Location(x, y, z, dimension, 0, 0))) {
			return location;
		}
		if (y < 100) {
			y = 100;
		}
		if (dimension == 1) {
			location.x = x = 0.5;
			location.z = z = 0.5;
		}
		int maxHeight = MyPlugin.getInstance().getServer().getMaxBuildHeight();
		if (dimension == -1) {
			maxHeight = 120;
		}
		for (int j = y; true; j--) {
			if (j == y + 1) {
				// looped all the way, end with original location
				break;
			}
			if (j < 0) {
				j = maxHeight;
			}
			if (isSafeLocation(new MC_Location(x, j, z, dimension, 0, 0))) {
				location.y = j;
				break;
			}
		}
		return location;
	}

	public static boolean isSafeLocation(MC_Location location) {
		MC_World world = MyPlugin.getInstance().getServer().getWorld(location.dimension);
		int x = location.getBlockX();
		int y = location.getBlockY();
		int z = location.getBlockZ();
		MC_Block under = world.getBlockAt(x, y - 1, z);
		MC_Block feet = world.getBlockAt(x, y, z);
		MC_Block head = world.getBlockAt(x, y + 1, z);
		if (under == null || feet == null || head == null) {
			return false;
		}
		if (under.isSolid() && !feet.isSolid() && !head.isSolid() && !feet.isLiquid() && !head.isLiquid()) {
			return true;
		}
		return false;
	}

	/**
	 * Get a list of nearby players that are within a certain radius
	 * 
	 * @param player The player at the center of the radius
	 * @param radius The amount of blocks in all directions from the player
	 * @return List of MC_Player objects within the radius
	 */
	public static List<MC_Player> getNearbyPlayers(MC_Player player, Integer radius) {
		MC_World world = player.getWorld();
		MC_Location location = player.getLocation();
		int x = location.getBlockX();
		int z = location.getBlockZ();
		int maxX = x + radius;
		int minX = x - radius;
		int maxZ = z + radius;
		int minZ = z - radius;
		List<MC_Player> nearby = new ArrayList<MC_Player>();
		for (MC_Player target : MyPlugin.getInstance().getServer().getPlayers()) {
			if (decolorize(target.getCustomName()).equalsIgnoreCase(decolorize(player.getCustomName()))) {
				continue; // Do not count self
			}
			if (!target.getWorld().getName().equals(world.getName())) {
				continue; // Not in same world
			}
			MC_Location tLoc = target.getLocation();
			int tX = tLoc.getBlockX();
			int tZ = tLoc.getBlockZ();
			if (tX > maxX || tX < minX) {
				continue; // too far on X
			}
			if (tZ > maxZ || tZ < minZ) {
				continue; // too far on Z
			}
			nearby.add(target);
		}
		return nearby;
	}

	/**
	 * Converts epoch timestamp into human readable date format (MMM d h:mma)
	 * 
	 * @param epoch Long representing epoch timestamp
	 * @return Human readable date and time
	 */
	public static String epochToTimeStamp(long epoch) {
		// TODO convert for user timezones
		return (new SimpleDateFormat("MMM d h:mma")).format(new Date(epoch));
	}

	/**
	 * Returns a list of online player names that start with the given string name. This differs from Rainbow's method which checks anywhere in the string for a match instead of starts with.
	 * 
	 * @param name The beginning of the player names try to match
	 * @return List of online player names
	 */
	public static List<String> getMatchingOnlinePlayerNames(String name) {
		List<String> matches = new ArrayList<String>();
		if (name == null) {
			name = "";
		}
		name = name.toLowerCase().trim();
		for (MC_Player player : MyPlugin.getInstance().getServer().getPlayers()) {
			if (decolorize(player.getCustomName().toLowerCase()).startsWith(name)) {
				matches.add(decolorize(player.getCustomName()));
			} else if (decolorize(player.getName().toLowerCase()).startsWith(name)) {
				matches.add(decolorize(player.getName()));
			}
		}
		return matches;
	}

	/**
	 * Get an MC_Player object for a player by name. This method checks real names and custom names and ignores color/style codes
	 * 
	 * @param name Name of player
	 * @return MC_Player of player, or null if no player found
	 */
	public static MC_Player getPlayer(String name) {
		List<MC_Player> online = MyPlugin.getInstance().getServer().getPlayers();
		for (MC_Player player : online) {
			if (decolorize(player.getCustomName().toLowerCase()).equals(name.toLowerCase())) {
				return player;
			}
			if (decolorize(player.getName().toLowerCase()).equals(name.toLowerCase())) {
				return player;
			}
		}
		return null;
	}

	/**
	 * Get an MC_Player object for a player by UUID.
	 * 
	 * @param uuid UUID of player
	 * @return MC_Player of player, or null if no player found
	 */
	public static MC_Player getPlayer(UUID uuid) {
		List<MC_Player> online = MyPlugin.getInstance().getServer().getPlayers();
		for (MC_Player player : online) {
			if (player.getUUID().equals(uuid)) {
				return player;
			}
		}
		return null;
	}

	/**
	 * Get an MC_Player object for an offline player by UUID.
	 * 
	 * @param uuid UUID of player
	 * @return MC_Player of player, or null if no player found
	 */
	public static MC_Player getOfflinePlayer(UUID uuid) {
		List<MC_Player> online = MyPlugin.getInstance().getServer().getOfflinePlayers();
		for (MC_Player player : online) {
			if (player.getUUID().equals(uuid)) {
				return player;
			}
		}
		return null;
	}

	/**
	 * Get an MC_Player object for an offline player by name. This method checks real names and custom names and ignores color/style codes
	 * 
	 * @param name Name of player
	 * @return MC_Player of player, or null if no player found
	 */
	public static MC_Player getOfflinePlayer(String name) {
		List<MC_Player> online = MyPlugin.getInstance().getServer().getOfflinePlayers();
		for (MC_Player player : online) {
			if (decolorize(player.getCustomName().toLowerCase()).equals(name.toLowerCase())) {
				return player;
			}
			if (decolorize(player.getName().toLowerCase()).equals(name.toLowerCase())) {
				return player;
			}
		}
		return null;
	}

	/**
	 * Send a message to a player. Any raw text color codes will be converted to Minecraft color codes
	 * 
	 * @param uuid UUID of online player to send to
	 * @param message Message to send
	 */
	public static void sendMessage(UUID uuid, String message) {
		MC_Player player = getPlayer(uuid);
		if (player == null) {
			return;
		}
		sendMessage(player, message);
	}

	/**
	 * Send a message to a player or console. If player, any raw text color codes will be converted to Minecraft color codes
	 * 
	 * @param player MC_Player to send to, or null to send to console
	 * @param message Message to send
	 */
	public static void sendMessage(MC_Player player, String message) {
		if (message == null || message.equals("")) {
			return;
		}
		if (player == null) {
			// Console or CommandBlock
			System.out.println(colorizeConsole(message));
			return;
		}
		player.sendMessage(colorize(message));
	}

	/**
	 * Get the spawn location for a world.
	 * 
	 * @param world The MC_World to get the spawn point of
	 * @return MC_Location of the spawn point
	 */
	public static MC_Location getWorldSpawn(MC_World world) {
		MC_Location worldSpawn = MyPlugin.getInstance().getDataConfig().getLocation(getWorldName(world).toLowerCase() + "-spawn", null);
		if (worldSpawn == null) {
			worldSpawn = world.getSpawnLocation();
		}
		return getSafeLocation(worldSpawn);
	}

	/**
	 * Set the spawn location for a world
	 * 
	 * @param world The MC_World to set the spawn point of
	 * @param location The spawn point
	 */
	public static void setWorldSpawn(MC_World world, MC_Location location) {
		BaseConfig config = MyPlugin.getInstance().getDataConfig();
		config.setLocation(getWorldName(world).toLowerCase() + "-spawn", location);
		config.save();
	}

	/**
	 * Get the spawn location for the entire server. There is only 1 of these per server.
	 * 
	 * @return MC_Location of the spawn point
	 */
	public static MC_Location getServerSpawn() {
		MC_Location serverSpawn = MyPlugin.getInstance().getDataConfig().getLocation("spawn");
		if (serverSpawn == null) {
			serverSpawn = MyPlugin.getInstance().getServer().getWorld(0).getSpawnLocation();
		}
		return getSafeLocation(serverSpawn);
	}

	/**
	 * Set the spawn location for the entire server. This is only 1 of these per server.
	 * 
	 * @param location The spawn point
	 */
	public static void setServerSpawn(MC_Location location) {
		BaseConfig config = MyPlugin.getInstance().getDataConfig();
		config.setLocation("spawn", location);
		config.save();
	}

	/**
	 * Get world object by name
	 * 
	 * @param name Name of world
	 * @return MC_World object, or null if no world found
	 */
	public static MC_World getWorld(String name) {
		for (WorldServer ws : MinecraftServer.getServer().worldServers) {
			MC_World world = new WorldWrapper(ws);
			if (getWorldName(world).equalsIgnoreCase(name)) {
				return world;
			}
		}
		return null;
	}

	/**
	 * Get world object by dimension
	 * 
	 * @param dimension Dimension of world
	 * @return MC_World object, or null if no world found
	 */
	public static MC_World getWorld(int dimension) {
		return MyPlugin.getInstance().getServer().getWorld(dimension);
	}

	/**
	 * Get the proper name of the world
	 * 
	 * @return Proper world name
	 */
	public static String getWorldName(MC_World world) {
		String correctName = MinecraftServer.getServer().worldServers[0].getWorldData().getLevelName();
		return correctName + world.getName().substring(5);
	}

	/**
	 * Get a list of world names
	 * 
	 * @return List of world names
	 */
	public static List<String> getWorldNames() {
		List<String> worlds = new ArrayList<String>();
		for (WorldServer ws : MinecraftServer.getServer().worldServers) {
			worlds.add(getWorldName(new WorldWrapper(ws)));
		}
		return worlds;
	}

	/**
	 * Get a skull of an entity (player or mob)
	 * 
	 * @param entity Entity to get the skull of
	 * @return Skull of entity
	 */
	public static Skull getSkull(MC_Entity entity) {
		Skull skull = null;
		String entityType = entity.getType().toString();
		if (entity.getType().equals(MC_EntityType.SKELETON)) {
			if (((MC_Skeleton) entity).getSkeletonType().equals(MC_SkeletonType.WITHER_SKELETON)) {
				entityType = "WITHERSKELETON";
			}
		}
		try {
			skull = Skull.valueOf(entityType);
		} catch (Exception ignore) {
		}
		return skull;
	}

	/**
	 * Convert a Skull into an MC_ItemStack
	 * 
	 * @param skull The skull to convert
	 * @param victimName The name of the victim (or mobtype name)
	 * @param headLore The lore displayed on the skull item under the name
	 * @return MC_ItemStack of the skull
	 */
	public static MC_ItemStack getSkullItem(Skull skull, String victimName, String headLore) {
		MC_ItemStack skullItem = MyPlugin.getInstance().getServer().createItemStack(MC_ItemType.SKELETON_SKULL, 1, skull.getType());
		skullItem.setSkullOwner(skull.getSkin());
		skullItem.setCustomName(victimName);
		skullItem.setLore(Arrays.asList(Pl3xLibs.colorize(headLore)));
		return skullItem;
	}

	/**
	 * Send a block change packet to player
	 * 
	 * @param player Player to recieve packet
	 * @param loc Location of block to change
	 * @param block Block data to set
	 */
	public static void sendBlockChange(MC_Player player, MC_Location loc, MC_Block block) {
		IntegerCoordinates coords = new IntegerCoordinates(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
		Packet_BlockChange packet = new Packet_BlockChange(((WorldWrapper) player.getWorld()).world, coords);
		packet.b = ((BlockWrapper) block).m_blockState;
		packet.a = coords;
		((PlayerWrapper) player).plr.plrConnection.sendPacket(packet);
	}

	/**
	 * Get a block from a location
	 * 
	 * @param location MC_Location of block
	 * @return MC_Block at location
	 */
	public static MC_Block getBlock(MC_Location location) {
		return getWorld(location.dimension).getBlockAt(location);
	}

	/**
	 * Get the schedule manager
	 * 
	 * @return Scheduler
	 */
	public static Scheduler getScheduler() {
		return MyPlugin.getInstance().getScheduler();
	}

	/**
	 * Get a customized logger for your plugin. Complete with timestamp and plugin signature for clear identification.
	 * 
	 * @param pluginInfo Your plugin's PluginInfo object
	 * @return A customized Logger
	 */
	public static Logger getLogger(PluginInfo pluginInfo) {
		return MyPlugin.getInstance().getLogger(pluginInfo);
	}
}
