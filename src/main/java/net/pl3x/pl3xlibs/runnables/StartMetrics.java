package net.pl3x.pl3xlibs.runnables;

import java.io.IOException;

import _Pl3xLibs.MyPlugin;
import net.pl3x.pl3xlibs.MetricsLite;
import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;

public class StartMetrics extends Pl3xRunnable {
	private MyPlugin plugin;
	private MetricsLite metrics;

	public StartMetrics(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		if (plugin.getConfig().getBoolean("debug-mode")) {
			plugin.getLogger().debug("Starting metrics!");
		}
		try {
			metrics = new MetricsLite(MyPlugin.getInstance().getPluginInfo());
			metrics.start();
		} catch (IOException e) {
			plugin.getLogger().error("Failed to start Metrics: " + e.getMessage());
		}
	}
}
