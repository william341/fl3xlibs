package net.pl3x.pl3xlibs.commands;

import java.util.LinkedList;
import java.util.List;

import PluginReference.MC_Player;

public class Command extends BaseCommand {
	protected Command(String name, String description, String requiredPermission, String usage) {
		super(name, description, requiredPermission, usage);
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, LinkedList<String> args) {
		return getTabCompletionList(args, player);
	}

	@Override
	public void handleCommand(MC_Player player, LinkedList<String> args) throws Exception {
		if ("?".equals(args.peek())) {
			getHelpLine(player);
			return;
		}
		handleCommand(args, player);
	}

	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		return null;
	}

	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
	}
}
