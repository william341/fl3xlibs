package net.pl3x.pl3xlibs.commands;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class CommandHandler implements MC_Command {
	private String name;
	private String description;
	private String usage;
	private String requiredPermission;

	protected CommandHandler(String name, String description, String requiredPermission, String usage) {
		this.name = name;
		this.description = description;
		this.usage = usage;
		this.requiredPermission = requiredPermission;
	}

	public String getName() {
		return name == null || name.equals("") ? "perm" : name;
	}

	public String getDescription() {
		return description;
	}

	public String getUsage() {
		return usage;
	}

	public String getRequiredPermission() {
		return requiredPermission;
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("perms", "permission", "permissions");
	}

	@Override
	public String getCommandName() {
		return name;
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize(getUsage() + " &a- &d" + getDescription());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return getTabCompletionList(player, new LinkedList<String>(Arrays.asList(args)));
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (player != null && getRequiredPermission() != null && !player.hasPermission(getRequiredPermission())) {
			Pl3xLibs.sendMessage(player, "&4You do not have permission for that command!");
			return;
		}
		try {
			handleCommand(player, new LinkedList<String>(Arrays.asList(args)));
		} catch (Exception e) {
			if (e.getMessage() == null || e.getMessage().equals("")) {
				Pl3xLibs.sendMessage(player, "&4Unknown error has occured!");
			} else {
				Pl3xLibs.sendMessage(player, e.getMessage());
			}
			e.printStackTrace();
		}
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null || requiredPermission == null) {
			return true;
		}
		return player.hasPermission(requiredPermission);
	}

	public List<String> getTabCompletionList(MC_Player player, LinkedList<String> args) {
		return null;
	}

	public void handleCommand(MC_Player player, LinkedList<String> args) throws Exception {
		return;
	}
}
