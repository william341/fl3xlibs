package net.pl3x.pl3xlibs.commands;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import net.pl3x.pl3xlibs.Pl3xLibs;
import PluginReference.MC_Player;

public class BaseCommand extends CommandHandler {
	private final TreeMap<String, CommandHandler> subCommands = new TreeMap<String, CommandHandler>();

	public BaseCommand(String name, String description, String requiredPermission, String usage) {
		super(name, description, requiredPermission, usage);
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, LinkedList<String> args) {
		if (args.size() > 1) {
			CommandHandler subCmd = subCommands.get(args.peek().toLowerCase());
			if (subCmd != null && (player == null || subCmd.getRequiredPermission() == null || player.hasPermission(subCmd.getRequiredPermission()))) {
				args.pop(); // pop the subcommand from args
				return subCmd.getTabCompletionList(player, args);
			}
			return null;
		} else {
			List<String> results = new ArrayList<String>();
			for (Map.Entry<String, CommandHandler> cmdPair : subCommands.entrySet()) {
				if (cmdPair.getKey().startsWith(args.peek().toLowerCase())) {
					if (player == null || cmdPair.getValue().getRequiredPermission() == null || player.hasPermission(cmdPair.getValue().getRequiredPermission())) {
						results.add(cmdPair.getKey());
					}
				}
			}
			return results;
		}
	}

	@Override
	public void handleCommand(MC_Player player, LinkedList<String> args) throws Exception {
		if (args.size() > 0) {
			CommandHandler subCmd = subCommands.get(args.peek().toLowerCase());
			if (subCmd != null) {
				if (player == null || subCmd.getRequiredPermission() == null || player.hasPermission(subCmd.getRequiredPermission())) {
					args.pop(); // pop the subcommand from args
					subCmd.handleCommand(player, args);
					return;
				}
				Pl3xLibs.sendMessage(player, "&4You do not have permission for that command!");
				return;
			}
			Pl3xLibs.sendMessage(player, "&4Unknown subcommand!");
		}
		showSubCommands(player);
	}

	public void showSubCommands(MC_Player player) {
		Pl3xLibs.sendMessage(player, "&dAvailable subcommands:");
		for (CommandHandler handler : subCommands.values()) {
			if (player == null || handler.getRequiredPermission() == null || player.hasPermission(handler.getRequiredPermission())) {
				Pl3xLibs.sendMessage(player, handler.getName() + "&a- &d" + handler.getDescription());
			}
		}
	}

	public void registerSubcommand(CommandHandler commandHandler) {
		subCommands.put(commandHandler.getName().toLowerCase(), commandHandler);
	}
}
