package net.pl3x.pl3xlibs;

import java.text.SimpleDateFormat;
import java.util.Date;

import PluginReference.PluginInfo;

public class Logger {
	private BetterPluginInfo pluginInfo;

	public Logger(PluginInfo pluginInfo) {
		this.pluginInfo = new BetterPluginInfo(pluginInfo);
	}

	public void info(String message) {
		send("&3[&rINFO&3] &r" + message);
	}

	public void warn(String message) {
		send("&3[&4WARN&3] &r" + message);
	}

	public void error(String message) {
		send("&3[&e!!! &cERROR &e!!!&3] &r" + message);
	}

	public void debug(String message) {
		send("&3[&6DEBUG&3] &r" + message);
	}

	private void send(String message) {
		System.out.println(Pl3xLibs.colorizeConsole(getPrefix() + message + "&r"));
	}

	private String getPrefix() {
		return "&3[&8" + getTime() + "&3] [&d" + pluginInfo.getName() + "&3] &r";
	}

	private String getTime() {
		Date time = new Date();
		String hours = new SimpleDateFormat("HH").format(time);
		String minutes = new SimpleDateFormat("mm").format(time);
		String seconds = new SimpleDateFormat("ss").format(time);
		return hours + "&6:&8" + minutes + "&6:&8" + seconds;
	}
}
