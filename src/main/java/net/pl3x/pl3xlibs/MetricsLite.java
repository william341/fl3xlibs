package net.pl3x.pl3xlibs;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

import net.pl3x.pl3xlibs.configuration.BaseConfig;
import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import net.pl3x.pl3xlibs.scheduler.Task;
import PluginReference.PluginInfo;
import _Pl3xLibs.MyPlugin;

public class MetricsLite {
	private static int REVISION = 7;
	private static String BASE_URL = "http://report.mcstats.org";
	private static String REPORT_URL = "/plugin/%s";
	private static int PING_INTERVAL = 15;
	private Task task = null;
	private String guid;
	private boolean debug = false;
	private BetterPluginInfo betterPluginInfo;

	public MetricsLite(PluginInfo pluginInfo) throws IOException {
		this.betterPluginInfo = new BetterPluginInfo(pluginInfo);
		BaseConfig config = new BaseConfig(MyPlugin.class, pluginInfo, ".." + File.separator + "Metrics" + File.separator, "config.ini");
		config.load(false);
		guid = config.get("guid");
		debug = config.getBoolean("debug-mode", false);
		if (guid == null || guid.equals("")) {
			guid = UUID.randomUUID().toString();
			config.set("guid", guid);
			config.set("debug-mode", "false");
			config.save();
		}
	}

	public boolean start() {
		if (task != null) {
			return true;
		}
		task = Pl3xLibs.getScheduler().scheduleAsynchronousRepeatingTask(betterPluginInfo.getName(), new Pl3xRunnable() {
			private boolean firstPost = true;
			private PluginInfo pluginInfo = new PluginInfo();

			public void run() {
				pluginInfo.name = "Metrics";
				if (debug) {
					Pl3xLibs.getLogger(pluginInfo).debug("Sending stats to MCStats.org");
				}
				try {
					postPlugin(!firstPost);
					firstPost = false;
				} catch (IOException e) {
					Pl3xLibs.getLogger(pluginInfo).warn("Metrics error: " + e.getLocalizedMessage());
				}
			}
		}, 0, PING_INTERVAL * 1200);
		return true;
	}

	public void enable() throws IOException {
		if (task == null) {
			start();
		}
	}

	public void disable() throws IOException {
		if (task != null) {
			task.cancel();
			task = null;
		}
	}

	private void postPlugin(boolean isPing) throws IOException {
		String pluginName = betterPluginInfo.getName();
		boolean onlineMode = MyPlugin.getInstance().getServer().getOnlineMode();
		String pluginVersion = betterPluginInfo.getVersion();
		String serverVersion = "RainbowMod v" + MyPlugin.getInstance().getServer().getRainbowVersion();
		int playersOnline = MyPlugin.getInstance().getServer().getPlayers().size();
		StringBuilder json = new StringBuilder(1024);
		json.append('{');
		appendJSONPair(json, "guid", guid);
		appendJSONPair(json, "plugin_version", pluginVersion);
		appendJSONPair(json, "server_version", serverVersion);
		appendJSONPair(json, "players_online", Integer.toString(playersOnline));
		String osname = System.getProperty("os.name");
		String osarch = System.getProperty("os.arch");
		String osversion = System.getProperty("os.version");
		String java_version = System.getProperty("java.version");
		int coreCount = Runtime.getRuntime().availableProcessors();
		if (osarch.equals("amd64")) {
			osarch = "x86_64";
		}
		appendJSONPair(json, "osname", osname);
		appendJSONPair(json, "osarch", osarch);
		appendJSONPair(json, "osversion", osversion);
		appendJSONPair(json, "cores", Integer.toString(coreCount));
		appendJSONPair(json, "auth_mode", onlineMode ? "1" : "0");
		appendJSONPair(json, "java_version", java_version);
		if (isPing) {
			appendJSONPair(json, "ping", "1");
		}
		json.append('}');
		URL url = new URL(BASE_URL + String.format(REPORT_URL, urlEncode(pluginName)));
		URLConnection connection;
		if (isMineshafterPresent()) {
			connection = url.openConnection(Proxy.NO_PROXY);
		} else {
			connection = url.openConnection();
		}
		byte[] compressed = gzip(json.toString());
		connection.addRequestProperty("User-Agent", "MCStats/" + REVISION);
		connection.addRequestProperty("Content-Type", "application/json");
		connection.addRequestProperty("Content-Encoding", "gzip");
		connection.addRequestProperty("Content-Length", Integer.toString(compressed.length));
		connection.addRequestProperty("Accept", "application/json");
		connection.addRequestProperty("Connection", "close");
		connection.setDoOutput(true);
		OutputStream os = connection.getOutputStream();
		os.write(compressed);
		os.flush();
		final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String response = reader.readLine();
		os.close();
		reader.close();
		if (response == null || response.startsWith("ERR") || response.startsWith("7")) {
			if (response == null) {
				response = "null";
			} else if (response.startsWith("7")) {
				response = response.substring(response.startsWith("7,") ? 2 : 1);
			}
			throw new IOException(response);
		}
	}

	public static byte[] gzip(String input) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GZIPOutputStream gzos = null;
		try {
			gzos = new GZIPOutputStream(baos);
			gzos.write(input.getBytes("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (gzos != null)
				try {
					gzos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return baos.toByteArray();
	}

	private boolean isMineshafterPresent() {
		try {
			Class.forName("mineshafter.MineServer");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private static void appendJSONPair(StringBuilder json, String key, String value) throws UnsupportedEncodingException {
		boolean isValueNumeric = false;
		try {
			if (value.equals("0") || !value.endsWith("0")) {
				Double.parseDouble(value);
				isValueNumeric = true;
			}
		} catch (NumberFormatException e) {
			isValueNumeric = false;
		}
		if (json.charAt(json.length() - 1) != '{') {
			json.append(',');
		}
		json.append(escapeJSON(key));
		json.append(':');
		if (isValueNumeric) {
			json.append(value);
		} else {
			json.append(escapeJSON(value));
		}
	}

	private static String escapeJSON(String text) {
		StringBuilder builder = new StringBuilder();
		builder.append('"');
		for (int index = 0; index < text.length(); index++) {
			char chr = text.charAt(index);
			switch (chr) {
				case '"':
				case '\\':
					builder.append('\\');
					builder.append(chr);
					break;
				case '\b':
					builder.append("\\b");
					break;
				case '\t':
					builder.append("\\t");
					break;
				case '\n':
					builder.append("\\n");
					break;
				case '\r':
					builder.append("\\r");
					break;
				default:
					if (chr < ' ') {
						String t = "000" + Integer.toHexString(chr);
						builder.append("\\u" + t.substring(t.length() - 4));
					} else {
						builder.append(chr);
					}
					break;
			}
		}
		builder.append('"');
		return builder.toString();
	}

	private static String urlEncode(final String text) throws UnsupportedEncodingException {
		return URLEncoder.encode(text, "UTF-8");
	}
}
