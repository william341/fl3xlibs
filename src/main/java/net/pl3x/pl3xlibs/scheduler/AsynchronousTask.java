package net.pl3x.pl3xlibs.scheduler;

public class AsynchronousTask extends Task {
	public AsynchronousTask(Pl3xRunnable runnable, Integer delay) {
		this(runnable, delay, -1);
	}

	public AsynchronousTask(Pl3xRunnable runnable, Integer delay, Integer period) {
		super(runnable, delay, period);
	}
}
