package net.pl3x.pl3xlibs;

import java.io.File;
import java.util.List;

import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class BetterPluginInfo extends PluginInfo {

	// hide default constructor from public use
	@SuppressWarnings("unused")
	private BetterPluginInfo() {
	}

	public BetterPluginInfo(PluginInfo diw) {
		this.name = diw.name;
		this.description = diw.description;
		this.version = diw.version;
		this.path = diw.path;
		this.eventSortOrder = diw.eventSortOrder;
		this.pluginNamesINeedToGetEventsAfter = diw.pluginNamesINeedToGetEventsAfter;
		this.pluginNamesINeedToGetEventsBefore = diw.pluginNamesINeedToGetEventsBefore;
		this.ref = diw.ref;
		this.optionalData = diw.optionalData;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getVersion() {
		return version;
	}

	public String getPluginDirectory() {
		String pluginDirectory;
		if (optionalData.containsKey("plugin-directory")) {
			pluginDirectory = optionalData.get("plugin-directory");
		} else {
			pluginDirectory = "plugins_mod" + File.separator + getName() + File.separator;
		}
		return pluginDirectory;
	}

	public String getPath() {
		return path;
	}

	public double getEventSortOrder() {
		return eventSortOrder;
	}

	public List<String> getEventsBefore() {
		return pluginNamesINeedToGetEventsBefore;
	}

	public List<String> getEventsAfter() {
		return pluginNamesINeedToGetEventsAfter;
	}

	public PluginBase getPluginBase() {
		return ref;
	}
}
