package net.pl3x.pl3xlibs;

import PluginReference.MC_Location;

public class Location {
	private double x;
	private double y;
	private double z;
	private float yaw;
	private float pitch;
	private World world;

	/**
	 * Represents a location in the world
	 * 
	 * @param world The world location is in
	 * @param x X coordinate in world
	 * @param y Y coordinate in world
	 * @param z Z coordinate in world
	 * @param yaw Horizontal angle
	 * @param pitch Vertical angle
	 */
	public Location(World world, double x, double y, double z, float yaw, float pitch) {
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
	}

	/**
	 * Represents a location in the world
	 * 
	 * @param world The world location is in
	 * @param x X coordinate in world
	 * @param y Y coordinate in world
	 * @param z Z coordinate in world
	 */
	public Location(World world, double x, double y, double z) {
		this(world, x, y, z, 0, 0);
	}

	/**
	 * Represents a location in the world
	 * 
	 * @param world The world location is in
	 * @param x X coordinate in world
	 * @param y Y coordinate in world
	 * @param z Z coordinate in world
	 */
	public Location(World world, int x, int y, int z) {
		this(world, (double) x, (double) y, (double) z);
	}

	/**
	 * Represents a location in the world
	 * 
	 * @param loc An MC_Location to convert
	 */
	public Location(MC_Location loc) {
		this(new World(loc.dimension), loc.x, loc.y, loc.z, loc.yaw, loc.pitch);
	}

	/**
	 * Get the X location
	 * 
	 * @return X Location
	 */
	public double getX() {
		return x;
	}

	/**
	 * Set the X location
	 * 
	 * @param x X location
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Get the Y location
	 * 
	 * @return Y Location
	 */
	public double getY() {
		return y;
	}

	/**
	 * Set the Y location
	 * 
	 * @param y Y location
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * Get the Z location
	 * 
	 * @return Z Location
	 */
	public double getZ() {
		return z;
	}

	/**
	 * Set the Z location
	 * 
	 * @param z Z location
	 */
	public void setZ(double z) {
		this.z = z;
	}

	/**
	 * Get the yaw (horizontal angle)
	 * 
	 * @return Yaw
	 */
	public float getYaw() {
		return yaw;
	}

	/**
	 * Set the yaw (horizontal angle)
	 * 
	 * @param yaw Yaw to set
	 */
	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	/**
	 * Get the pitch (vertical angle)
	 * 
	 * @return Pitch
	 */
	public float getPitch() {
		return pitch;
	}

	/**
	 * Set the pitch (vertical angle)
	 * 
	 * @param pitch Pitch to set
	 */
	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	/**
	 * Get the floored value of X
	 * 
	 * @return floored X coordinate
	 */
	public int getBlockX() {
		return (int) Math.floor(x);
	}

	/**
	 * Get the floored value of Y
	 * 
	 * @return floored Y coordinate
	 */
	public int getBlockY() {
		return (int) Math.floor(y);
	}

	/**
	 * Get the floored value of Z
	 * 
	 * @return floored Z coordinate
	 */
	public int getBlockZ() {
		return (int) Math.floor(z);
	}

	/**
	 * Get the block at this location
	 * 
	 * @return Block at location
	 */
	public Block getBlock() {
		return world.getBlockAt(this);
	}

	/**
	 * Get the world this location resides in
	 * 
	 * @return World containing this location
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * Get a copy of location
	 * 
	 * @return Copy of location
	 */
	public Location clone() {
		return new Location(getWorld(), getX(), getY(), getZ());
	}

	/**
	 * Compare location to another location
	 * 
	 * @param loc Location to compare to
	 * @return True if locations match, false if not
	 */
	public Boolean equals(Location loc) {
		return loc.x == x && loc.y == y && loc.z == z && loc.world.equals(world);
	}

	/**
	 * Get the distance between this location and another. The value of this method is not cached and uses a costly square-root function, so do not repeatedly call this method to get the location's magnitude. NaN will be returned if the inner result of the sqrt() function overflows, which will be caused if the distance is too long.
	 * 
	 * @param loc The other location
	 * @return The distance
	 */
	public double distance(Location loc) {
		return Math.sqrt(distanceSquared(loc));
	}

	/**
	 * Get the squared distance between this location and another
	 * 
	 * @param loc The other location
	 * @return The distance
	 */
	public double distanceSquared(Location loc) {
		if (loc.getWorld().equals(getWorld())) {
			throw new IllegalArgumentException("Cannot get distance between two worlds");
		}
		double dx = x - loc.x;
		double dy = y - loc.y;
		double dz = z - loc.z;
		return dx * dx + dy * dy + dz * dz;
	}

	/**
	 * Get a MC_Location object of this location
	 * 
	 * @return MC_Location
	 */
	public MC_Location getHandle() {
		return new MC_Location(x, y, z, world.getHandle().getDimension(), yaw, pitch);
	}
}
